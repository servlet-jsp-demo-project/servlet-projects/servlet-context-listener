package com.cdac.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBUtils {

	private static Connection connection;

	public static void createConnection(String driver, String url, String username, String password)
			throws ClassNotFoundException, SQLException {
		/*
		 * Load JDBC Driver
		 */
		Class.forName(driver);

		/*
		 * Get connection from DriverManager
		 */
		connection = DriverManager.getConnection(url, username, password);
	}

	public static Connection getDBConnection() {
		return connection;
	}

	public static void cleanUp() throws SQLException {
		connection.close();
	}

}
