package com.cdac.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.cdac.pojo.Customer;
import com.cdac.utils.DBUtils;

public class CustomerDaoImpl implements CustomerDao {
	private Connection connection = null;

	public CustomerDaoImpl() throws ClassNotFoundException, SQLException {
		connection = DBUtils.getDBConnection();
	}

	@Override
	public Customer authenticateCustomer(String email, String password) throws Exception {
		PreparedStatement prepareStatement = connection
				.prepareStatement("Select * from customers where email=? and password=?");
		prepareStatement.setString(1, email);
		prepareStatement.setString(2, password);

		ResultSet result = prepareStatement.executeQuery();
		Customer customer = null;
		if (result.next()) {
			customer = new Customer(result.getInt("id"), result.getString("name"), password, result.getString("mobile"),
					password, email, result.getDate("birth"));
		}
		result.close();

		prepareStatement.close();
		return customer;
	}

}
