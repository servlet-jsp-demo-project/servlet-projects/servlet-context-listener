package com.cdac.listener;

import java.sql.SQLException;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.cdac.utils.DBUtils;

@WebListener
public class ServlerContextListner implements ServletContextListener {

	public ServlerContextListner() {
		System.out.println("Parameterless Constructor : " + this.getClass().getName());
	}

	public void contextDestroyed(ServletContextEvent sce) {
		try {
			DBUtils.cleanUp();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void contextInitialized(ServletContextEvent sce) {

		/*
		 * get ServletContext object
		 */
		ServletContext servletContext = sce.getServletContext();

		/*
		 * Get Database properties from ServletContext object
		 */
		String driver = servletContext.getInitParameter("driver");
		String url = servletContext.getInitParameter("url");
		String username = servletContext.getInitParameter("username");
		String password = servletContext.getInitParameter("password");

		/*
		 * Create Database connection
		 */
		try {
			DBUtils.createConnection(driver, url, username, password);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}

}
